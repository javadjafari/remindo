package com.jai.remindo;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jai.remindo.data.AlarmReminderContract;

import static android.content.ContentValues.TAG;


public class AlarmCursorAdapter extends CursorAdapter {

    private TextView mTitleText, mDateText, mTimeText, mRepeatText;

    public AlarmCursorAdapter(Context context, Cursor c) {
        super(context, c, 0 );
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.reminder_row, parent, false);

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        mTitleText = view.findViewById(R.id.title);
        mDateText = view.findViewById(R.id.date);
        mTimeText = view.findViewById(R.id.time);
        mRepeatText=view.findViewById(R.id.repeat);

        int titleColumnIndex = cursor.getColumnIndex(AlarmReminderContract.AlarmReminderEntry.KEY_TITLE);
        int dateColumnIndex = cursor.getColumnIndex(AlarmReminderContract.AlarmReminderEntry.KEY_DATE);
        int timeColumnIndex = cursor.getColumnIndex(AlarmReminderContract.AlarmReminderEntry.KEY_TIME);
        int repeatColumnIndex = cursor.getColumnIndex(AlarmReminderContract.AlarmReminderEntry.KEY_REPEAT);


        String title = cursor.getString(titleColumnIndex);
        String date = cursor.getString(dateColumnIndex);
        String time = cursor.getString(timeColumnIndex);
        int repeat = cursor.getInt(repeatColumnIndex);


        mTitleText.setText(title);
        mDateText.setText(getString(date));
        mTimeText.setText(time);

        switch (repeat)
        { case 0:
                mRepeatText.setText(AlarmReminderContract.items[0]);
                break;
            case 1:
                mRepeatText.setText(AlarmReminderContract.items[1]);
                break;
            case 2:
                mRepeatText.setText(AlarmReminderContract.items[2]);
                break;
            case 3:
                mRepeatText.setText(AlarmReminderContract.items[3]);
                break;
            case 4:
                mRepeatText.setText(AlarmReminderContract.items[4]);
                break;
            case 5:
                mRepeatText.setText(AlarmReminderContract.items[5]);
                break;
        }

    }

    public StringBuffer getString(String text) {

        StringBuffer finalString;
        int index = 0;
        finalString = new StringBuffer();
        while (index < text.length()) {
            Log.i(TAG, "test = " + text.substring(index, Math.min(index + 7, text.length())));
            finalString.append(text.substring(index, Math.min(index + 7, text.length())) + "\n");
            index += 7;

        }
        return finalString;
    }

}

