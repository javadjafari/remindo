package com.jai.remindo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.LoaderManager;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.jai.remindo.data.AlarmReminderContract;
import com.jai.remindo.data.AlarmReminderDbHelper;
import com.jai.remindo.data.AlarmReminderProvider;
import com.jai.remindo.reminder.AlarmScheduler;
import com.jai.remindo.reminder.ReminderAlarmService;

import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    ListView listView;
    Button add;
    TextView addTime,addDate;
    EditText addTitle;
    Spinner spinner;
    BottomSheetBehavior behavior;

    AlarmCursorAdapter mCursorAdapter;
    AlarmReminderDbHelper alarmReminderDbHelper = new AlarmReminderDbHelper(this);

    private int mYear;
    private int mMonth;
    private int mDay;
    private int mHour;
    private int mMinute;
    private long mRepeatTime;
    public final String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    boolean isCollapsed = true;

    private static final int VEHICLE_LOADER = 0;

    private Calendar mCalendar, c;

    // Constant values in milliseconds
    private static final long milHour   = 3600000L;
    private static final long milDay    = 86400000L;
    private static final long milWeek   = 604800000L;
    private static final long milMonth  = 2592000000L;
    private static final long milYear   = 31536000000L;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init
        mYear   = Calendar.getInstance().get(Calendar.YEAR);
        mMonth  = Calendar.getInstance().get(Calendar.MONTH)+1;
        mDay    = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) ;
        mHour   = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) ;
        mMinute = Calendar.getInstance().get(Calendar.MINUTE);
        mCalendar = Calendar.getInstance();

        add      = findViewById(R.id.add);
        listView = findViewById(R.id.list_view);
        addTitle = findViewById(R.id.add_title);
        addDate  = findViewById(R.id.add_date);
        addTime  = findViewById(R.id.add_time);
        spinner  = findViewById(R.id.repeat);

        mCursorAdapter = new AlarmCursorAdapter(this, null);

        listView.setAdapter(mCursorAdapter);
        ArrayAdapter<String> adapter = new
                ArrayAdapter<>(this,R.layout.spinner_layout,AlarmReminderContract.items);

        spinner.setAdapter(adapter);
        addDate.setText(String.format(Locale.getDefault(),
                "%s %d, %d", MONTHS[mMonth - 1], mDay, mYear));
        addTime.setText(String.format(Locale.getDefault(),
                "%s:%d",mHour,mMinute));

        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
                if (newState == BottomSheetBehavior.STATE_EXPANDED)
                {
                    add.setText(R.string.save);
                    isCollapsed = false;
                }


                else if(newState == BottomSheetBehavior.STATE_COLLAPSED)
                {
                    add.setText(R.string.add);
                    isCollapsed = true;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }

        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isCollapsed)
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                else
                    if (addTitle.length() > 0 )
                         SaveReminder();
                    else
                        Toast.makeText(MainActivity.this, R.string.title_cant_be_null, Toast.LENGTH_SHORT).show();
            }
        });

        addTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePicker();
            }
        });
        addDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, final long id) {


                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Delete")
                        .setMessage("Delete this Reminder?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Uri currentVehicleUri = ContentUris.withAppendedId(AlarmReminderContract.AlarmReminderEntry.CONTENT_URI, id);
                                DeleteReminder(currentVehicleUri);

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(R.drawable.ic_delete_)
                        .show();
                return false;
            }
        });
        getLoaderManager().initLoader(VEHICLE_LOADER, null, this);


    }
    // On clicking the save button
    public void SaveReminder() {

        ContentValues values = new ContentValues();
        int position = spinner.getSelectedItemPosition();

        values.put(AlarmReminderContract.AlarmReminderEntry.KEY_TITLE, addTitle.getText().toString());
        values.put(AlarmReminderContract.AlarmReminderEntry.KEY_DATE, MONTHS[mMonth] + " " + mDay + " " + mYear);
        values.put(AlarmReminderContract.AlarmReminderEntry.KEY_TIME, mHour + ":" + mMinute);
        values.put(AlarmReminderContract.AlarmReminderEntry.KEY_REPEAT, position);

        // Set up calender for creating the notification
        mCalendar.set(Calendar.MONTH, --mMonth);
        mCalendar.set(Calendar.YEAR, mYear);
        mCalendar.set(Calendar.DAY_OF_MONTH, mDay);
        mCalendar.set(Calendar.HOUR_OF_DAY, mHour);
        mCalendar.set(Calendar.MINUTE, mMinute);
        mCalendar.set(Calendar.SECOND, 0);


        long selectedTimestamp = mCalendar.getTimeInMillis();


        // Check repeat length


        if (position == 1)
            mRepeatTime =  milHour;
        else if (position == 2)
            mRepeatTime =  milDay;
        else if (position == 3)
            mRepeatTime = milWeek;
        else if (position == 4)
            mRepeatTime =  milMonth;
        else if (position == 5)
            mRepeatTime = milYear;

        Uri newUri = getContentResolver().insert(AlarmReminderContract.AlarmReminderEntry.CONTENT_URI, values);

        // Show a toast message depending on whether or not the insertion was successful.
        if (newUri == null) {
            // If the new content URI is null, then there was an error with insertion.
            Toast.makeText(this, getString(R.string.editor_insert_reminder_failed),
                    Toast.LENGTH_SHORT).show();
        } else {
            // Otherwise, the insertion was successful and we can display a toast.
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            closeKeyboard();
            Toast.makeText(this, getString(R.string.editor_insert_reminder_successful),
                    Toast.LENGTH_SHORT).show();

            // Create a new notification
            if (position == 0) {
                new AlarmScheduler().setAlarm(getApplicationContext(), selectedTimestamp, newUri);
            }
             else {
                new AlarmScheduler().setRepeatAlarm(getApplicationContext(), selectedTimestamp, newUri, mRepeatTime);
            }

        }

    }
    private void DeleteReminder(Uri uri) {
        // Only perform the delete if this is an existing reminder.
        if (uri != null) {
            // Call the ContentResolver to delete the reminder at the given content URI.
            // Pass in null for the selection and selection args because the mCurrentreminderUri
            // content URI already identifies the reminder that we want.
            int rowsDeleted = getContentResolver().delete(uri, null, null);
            new AlarmScheduler().cancelAlarm(this,uri);

            // Show a toast message depending on whether or not the delete was successful.
            if (rowsDeleted == 0) {
                // If no rows were deleted, then there was an error with the delete.
                Toast.makeText(this, getString(R.string.editor_delete_reminder_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                // Otherwise, the delete was successful and we can display a toast.
                Toast.makeText(this, getString(R.string.editor_delete_reminder_successful),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showDatePicker() {
        c = Calendar.getInstance();
        int mYearParam = mYear;
        int mMonthParam = mMonth-1;
        int mDayParam = mDay;

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        mMonth = monthOfYear + 1;
                        mYear=year;
                        mDay=dayOfMonth;

                        addDate.setText(String.format(Locale.getDefault(),"%s %d, %d", MONTHS[mMonth - 1], mDay, mYear));

                    }
                }, mYearParam, mMonthParam, mDayParam);

        datePickerDialog.show();
    }
    private void showTimePicker() {

        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int pHour,
                                          int pMinute) {

                        mHour = pHour;
                        mMinute = pMinute;
                        addTime.setText(String.format(Locale.getDefault(),"%d:%d", mHour, mMinute));
                    }
                }, mHour,mMinute , true);

        timePickerDialog.show();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = {
                AlarmReminderContract.AlarmReminderEntry._ID,
                AlarmReminderContract.AlarmReminderEntry.KEY_TITLE,
                AlarmReminderContract.AlarmReminderEntry.KEY_DATE,
                AlarmReminderContract.AlarmReminderEntry.KEY_TIME,
                AlarmReminderContract.AlarmReminderEntry.KEY_REPEAT,

        };

        return new CursorLoader(this,
                AlarmReminderContract.AlarmReminderEntry.CONTENT_URI,
                projection,
                null,
                null,
                null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);

    }

    private void closeKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}