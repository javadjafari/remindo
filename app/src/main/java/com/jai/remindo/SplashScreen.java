package com.jai.remindo;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SplashScreen extends AppCompatActivity {
    LinearLayout logoLayout;
    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        logoLayout = findViewById(R.id.logo_layout);
        logo = findViewById(R.id.img_logo);

        startAnim(logoLayout,logo);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashScreen.this , MainActivity.class);
                startActivity(intent);
                finish();
            }
        },3000);


    }


    private void startAnim(final View view, final View view2) {
        view.setAlpha(0f);
        view.animate()
                .alpha(1f)
                .setDuration(1500)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ObjectAnimator rotate = ObjectAnimator.ofFloat(view2, "rotation", 0f, 8f, 0f, -8f, 0f);
                        rotate.setRepeatCount(10);
                        rotate.setDuration(100);
                        rotate.start();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }
                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });

    }

}