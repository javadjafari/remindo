package com.jai.remindo.reminder;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.jai.remindo.MainActivity;
import com.jai.remindo.R;
import com.jai.remindo.data.AlarmReminderContract;


public class ReminderAlarmService extends IntentService {
    private static final String TAG = ReminderAlarmService.class.getSimpleName();

    private static final int NOTIFICATION_ID = 42;

    Cursor cursor;

    //This is a deep link intent, and needs the task stack
    public static PendingIntent getReminderPendingIntent(Context context, Uri uri) {
        Intent action = new Intent(context, ReminderAlarmService.class);
        action.setData(uri);
        return PendingIntent.getService(context, 0, action, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public ReminderAlarmService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "com.jai.remindo",
                    TAG,
                    NotificationManager.IMPORTANCE_HIGH

            );

            manager.createNotificationChannel(channel);

            Uri uri = intent.getData();

            //Display a notification to view the task details
            Intent action = new Intent(this, MainActivity.class);
            action.setData(uri);
            PendingIntent operation = TaskStackBuilder.create(this)
                    .addNextIntentWithParentStack(action)
                    .getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            //Grab the task description
            if (uri != null) {
                cursor = getContentResolver().query(uri, null, null, null, null);
            } else
            {
                Toast.makeText(this, "Uri is Null", Toast.LENGTH_SHORT).show();
            }

            String description = "";
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    description = AlarmReminderContract.getColumnString(cursor, AlarmReminderContract.AlarmReminderEntry.KEY_TITLE);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }

            Notification note = new Notification.Builder(this)
                    .setContentTitle("Reminder time is up!")
                    .setContentText(description)
                    .setSmallIcon(R.drawable.small_icon)
                    .setContentIntent(operation)
                    .setAutoCancel(true)
                    .setChannelId("com.jai.remindo")
                    .build();

            manager.notify(NOTIFICATION_ID, note);
        }
    }
}